from flask import Flask
from flask import request, jsonify
from flask_cors import CORS, cross_origin

# setup the application
app = Flask(__name__)
CORS(app)

# Make the WSGI interface available at the top level so wfastcgi can get it.
wsgi_app = app.wsgi_app

# Will just return hello work to user
@app.route('/', methods=['GET'])
@cross_origin()
def hello():
    """Renders a sample page."""
    return "Hello World!"

# Example usage - http://localhost:5555/api/values?id=5
@app.route('/api/values', methods=['GET'])
@cross_origin()
def get_values():
    """returns some values"""

    # Create an empty list for our results
    results = []

    # use requests.args to grabe thing after the ? in the url
    if 'id' in request.args:
        id = int(request.args['id'])
        results.append(id)
    else:
        return "Error: No id field provided. Please specify an id."

    # use jsonify to turn the data into json to transfer over the wire
    return jsonify(results)

# This is where the code is hit run you start it
if __name__ == '__main__':
    import os
    HOST = os.environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    app.run(HOST, PORT)
